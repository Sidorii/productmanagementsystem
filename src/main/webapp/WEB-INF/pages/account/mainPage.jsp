<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Hello Page</title>

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <style>

        .table-style {
            border-collapse: collapse;
            width: 100%;
            margin-bottom: 20px;
        }

        td {
            height: 50px;
            vertical-align: center;
            margin-inside: 5px;
        }

        tr:nth-child(even) {
            background-color: #d9edf7
        }

        th {
            padding: 3px;
            background-color: aliceblue;
            color: #31708f;
            height: 50px;
        }

        input {
            margin-top: 3px;
        }

        .error {
            padding: 15px;
            margin-bottom: 20px;
            border: 1px solid transparent;
            border-radius: 4px;
            color: #a94442;
            background-color: #f2dede;
            border-color: #ebccd1;
        }

        .msg {
            text-align: center;
            padding: 15px;
            margin-bottom: 20px;
            border: 1px solid transparent;
            border-radius: 4px;
            color: #31708f;
            background-color: #d9edf7;
            border-color: #bce8f1;
        }

        .login-box {
            width: 300px;
            padding: 20px;
            margin: 100px auto;
            background: #fff;
            -webkit-border-radius: 2px;
            -moz-border-radius: 2px;
            border: 3px solid #000;
        }

        .table-container {
            width: 96%;
            margin: 3%;
        }


    </style>
</head>

<body>
<c:url var="update" value="update"/>
<c:url value="delete" var="delete"/>

<c:if test="${not empty create}">
    <div class="${style}">
        <h4>${create}</h4>
    </div>
</c:if>

<c:if test="${not empty login}">
    <div class="msg">
        <h4>${login}</h4>
    </div>
</c:if>

<c:if test="${not empty updated_id}">
    <div class="${style}">
        <h4>${updated_id}</h4>
    </div>
</c:if>

<c:if test="${not empty deleted_id}">

    <div class="${style}">
        <h4>${deleted_id}</h4>
    </div>
</c:if>

<div>
    <table align="center" style="margin-top: 10px">
        <tr>
            <td>
                <form:form action="/account/create" method="get">
                    <input type="submit" value="Create new" class="btn btn-success" style="margin: 3px">
                </form:form>
            </td>
            <td>
                <c:url value="/j_spring_security_logout" var="logout"/>
                <form:form action="${logout}" method="post">
                    <input type="submit" value="Log out" class="btn btn-primary">
                </form:form>
            </td>
        </tr>
    </table>

</div>
<div class="table-container">
    <table align="center" border="true" class="table-style">
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Producer</th>
            <th>Price</th>
            <th>Description</th>
            <th>Action</th>
        </tr>
        <c:forEach var="product" items="${productsList}">
            <tr>
                <td align="center">${product.id}</td>
                <td align="center">${product.name}</td>
                <td align="center">${product.producer}</td>
                <td align="center">${product.price}</td>
                <td style="padding: 5px; max-width: 200px">${product.description}</td>
                <td align="center">
                    <form:form action="${update}" method="post">
                        <input type="hidden" name="product" value="${product.id}">
                        <input type="submit" value="Update" class="btn btn-success">
                    </form:form>
                    <form:form action="${delete}" method="post">
                        <input type="hidden" name="product_id" value="${product.id}">
                        <input type="submit" value="Delete" class="btn btn-danger">
                    </form:form>
                </td>
            </tr>
        </c:forEach>
    </table>
</div>
</body>
</html>
