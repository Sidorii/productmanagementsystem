<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <title>Create Product</title>
    <style>
        .create-box {
            width: 500px;
            padding: 20px;
            margin: 60px auto;
            background: #fff;
            border-radius: 6px;
            -webkit-border-radius: 6px;
            -moz-border-radius: 6px;
            border: 3px solid #31708f;
        }
    </style>
</head>
<body>
<div align="center" style="padding: 10px" class="create-box">
    <form:form action="/account/create" method="POST" commandName="product">
        <h3>Create new product</h3>
        <div class="panel-group">
            <div class="panel panel-success">
                <div class="panel-head">
                    <form:label path="name">Name</form:label>
                </div>
                <div class="panel-body">
                    <form:input path="name" class="form-control"/>
                </div>
            </div>
            <div class="panel panel-success">
                <div class="panel-head">
                    <form:label path="producer">Producer</form:label>
                </div>
                <div class="panel-body">
                    <form:input path="producer" class="form-control"/>
                </div>
            </div>
            <div class="panel panel-success">
                <div class="panel-head">
                    <form:label path="price">Price</form:label>
                </div>
                <div class="panel-body">
                    <form:input path="price" class="form-control"/>
                </div>
            </div>
            <div class="panel panel-success">
                <div class="panel-head">
                    <form:label path="description">Description</form:label>
                </div>
                <div class="panel-body">
                    <form:textarea path="description" class="form-control" cssStyle="max-width: 400px"/>
                </div>
            </div>
        </div>

        <input type="submit" value="Submit" class="btn btn-success" align="center">
        <input type="button" value="Cancel" class="btn btn-default" align="center"
               onclick="location.href='/account/'">
    </form:form>
</div>
</body>
</html>
