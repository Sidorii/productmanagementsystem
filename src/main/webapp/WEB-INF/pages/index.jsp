<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <title>Login</title>
    <style>
        .error {
            padding: 15px;
            margin-bottom: 20px;
            border: 1px solid transparent;
            border-radius: 4px;
            color: #a94442;
            background-color: #f2dede;
            border-color: #ebccd1;
        }

        .msg {
            padding: 15px;
            margin-bottom: 20px;
            border: 1px solid transparent;
            border-radius: 4px;
            color: #31708f;
            background-color: #d9edf7;
            border-color: #bce8f1;
        }

        .login-box {
            width: 300px;
            padding: 20px;
            margin: 100px auto;
            background: #fff;
            border-radius: 6px;
            -webkit-border-radius: 6px;
            -moz-border-radius: 6px;
            border: 3px solid #31708f;
        }

    </style>
</head>
<body>
<c:url var="securityURL" value="j_spring_security_check"/>
<div class="login-box">
    <c:if test="${not empty error}">
        <div class="error">
            <h3>${error}</h3>
        </div>
    </c:if>
    <c:if test="${not empty logout}">
        <div class="msg">
            <h3>${logout}</h3>
        </div>
    </c:if>

    <form method="post" action="${securityURL}">
        <div class="form-group">
            <label class="sr-only">Username</label>
            <input type="text" name="username" class="form-control" placeholder="Enter username">
        </div>
        <div class="form-group">
            <label class="sr-only">Password</label>
            <input type="password" name="password" class="form-control" placeholder="Enter password">
            <input type="hidden"
                   name="${_csrf.parameterName}"
                   value="${_csrf.token}"/>
        </div>

        <input type="submit" value="Log in" class="btn btn-primary" align="center">
    </form>
</div>
</body>
</html>
