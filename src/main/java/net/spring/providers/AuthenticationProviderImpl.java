package net.spring.providers;

import net.spring.model.UsersAttempts;
import net.spring.service.authentication.UserAttemptsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.Date;

/**
 * Created by sidorii on 15.03.17.
 */
public class AuthenticationProviderImpl extends DaoAuthenticationProvider {

    private static final long TIME_TO_LOCK = 30000;
    private static final int MAX_ATTEMPTS = 3;


    private UserAttemptsService userAttemptsService;

    @Autowired
    @Qualifier("userAttemptsService")
    public void setUserDetailsLoader(UserAttemptsService userAttemptsService) {
        this.userAttemptsService = userAttemptsService;
    }

    @Autowired
    @Qualifier("jdbcUserDao")
    @Override
    public void setUserDetailsService(UserDetailsService userDetailsService) {
        super.setUserDetailsService(userDetailsService);
    }

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        UsersAttempts usersAttempts = null;

        try {
            System.out.println("Pre authentication point: username: " + authentication.getName() + " password: " +
                    authentication.getCredentials());

            usersAttempts = userAttemptsService.getUserAttempts(authentication.getName());

            validateUserOnAttempts(usersAttempts);

            authentication = super.authenticate(authentication);

            //if exception not throwing, then user is authenticated successfully
            userAttemptsService.resetFailAttempts(authentication.getName());

            return authentication;

        } catch (BadCredentialsException exc) {

            userAttemptsService.updateFailAttempts(authentication.getName());
            System.out.println("Bad credentials for user with name :" + authentication.getName());

            throw exc;

        } catch (LockedException exc) {

            System.out.println("User with name " + authentication.getName() + " is locked.");

            String error;

            if (usersAttempts != null) {
                Date lastAttempts = usersAttempts.getLastModifiedl();
                error = "User account is locked! <br><br>Username : "
                        + authentication.getName() + "<br>Last Attempts : " + lastAttempts;
            } else {
                error = exc.getMessage();
            }

            throw new LockedException(error);
        }
    }


    private void validateUserOnAttempts(UsersAttempts usersAttempts) {
        if (usersAttempts != null) {

            if (usersAttempts.getAttempts() >= MAX_ATTEMPTS) {
                if ((new Date().getTime() - usersAttempts.getLastModifiedl().getTime()) >= TIME_TO_LOCK) {
                    userAttemptsService.lockUser(usersAttempts.getUsername(), true);
                    userAttemptsService.resetFailAttempts(usersAttempts.getUsername());
                }else
                    userAttemptsService.lockUser(usersAttempts.getUsername(), false);
            }
        }else
            throw new BadCredentialsException("Invalid username!");
    }
}
