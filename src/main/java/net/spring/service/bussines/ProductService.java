package net.spring.service.bussines;

import net.spring.model.Product;

import java.util.List;

/**
 * Created by sidorii on 17.03.17.
 */
public interface ProductService {

    void addNewProduct(Product product);

    Product getProduct(int id);

    void updateIfExistProduct(Product product);

    void deleteIfExistProduct(int id);

    List<Product> getAllProducts();
}
