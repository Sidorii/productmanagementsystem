package net.spring.service.bussines;

import net.spring.dao.bussines.ProductDao;
import net.spring.model.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.sql.SQLDataException;
import java.util.List;

/**
 * Created by sidorii on 17.03.17.
 */
public class ProductServiceImpl implements ProductService {

    private ProductDao productDao;

    @Autowired
    public void setProductDao(ProductDao productDao) {
        this.productDao = productDao;
    }

    @Transactional
    @Override
    public void addNewProduct(Product product) {
        if (product == null)
            throw new IllegalArgumentException();

        productDao.addProduct(product);
    }

    @Override
    public Product getProduct(int id) {
        return productDao.getProduct(id);
    }

    @Transactional
    @Override
    public void updateIfExistProduct(Product product){

        if (getProduct(product.getId()) == null)
            throw new IllegalArgumentException();

        if (getProduct(product.getId()) != null)
            productDao.updateProduct(product);
        else
            System.out.println("Can`t execute update for product: " + product + ". Entry does mot exists.");
    }

    @Transactional
    @Override
    public void deleteIfExistProduct(int id) {
        Product product = getProduct(id);
        if (product != null)
            productDao.deleteProduct(id);
        else
            System.out.println("Can`t execute update for product: " + product + ". Entry does mot exists.");
    }

    @Transactional
    @Override
    public List<Product> getAllProducts() {
        return productDao.getAllProducts();
    }
}
