package net.spring.service.authentication;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.jdbc.JdbcDaoImpl;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by sidorii on 17.03.17.
 */
public class UserLoaderService implements UserService {


    private JdbcDaoImpl jdbcDao;

    @Qualifier("jdbcUserDao")
    @Autowired
    public void setJdbcDao(JdbcDaoImpl jdbcDao) {
        this.jdbcDao = jdbcDao;
    }

    @Transactional
    @Override
    public UserDetails loadUserByUsername(String username) {
        return jdbcDao.loadUserByUsername(username);
    }
}
