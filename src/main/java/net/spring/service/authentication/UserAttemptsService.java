package net.spring.service.authentication;

import net.spring.model.UsersAttempts;

/**
 * Created by sidorii on 17.03.17.
 */
public interface UserAttemptsService {

    void updateFailAttempts(String username);

    void resetFailAttempts(String username);

    UsersAttempts getUserAttempts(String username);

    void lockUser(String username, boolean isNotLocked);
}
