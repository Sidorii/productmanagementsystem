package net.spring.service.authentication;

import net.spring.dao.authentication.UserAttemptsDao;
import net.spring.model.UsersAttempts;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by sidorii on 17.03.17.
 */
public class UserAttemptsServiceImpl implements UserAttemptsService {


    private UserAttemptsDao userDao;


    @Autowired
    public void setUserDao(UserAttemptsDao userDao) {
        this.userDao = userDao;
    }

    @Transactional
    @Override
    public void updateFailAttempts(String username) {
        userDao.updateFailAttempts(username);
    }

    @Transactional
    @Override
    public void resetFailAttempts(String username) {
        userDao.resetFailAttempts(username);
    }


    @Override
    public UsersAttempts getUserAttempts(String username) {
        return userDao.getUserAttempts(username);
    }

    @Transactional
    @Override
    public void lockUser(String username, boolean isNotLocked) {
        userDao.lockUser(username, isNotLocked);
    }
}
