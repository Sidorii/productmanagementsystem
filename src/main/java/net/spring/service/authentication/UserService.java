package net.spring.service.authentication;

import org.springframework.security.core.userdetails.*;

/**
 * Created by sidorii on 17.03.17.
 */
public interface UserService extends org.springframework.security.core.userdetails.UserDetailsService {

    UserDetails loadUserByUsername(String username);

}
