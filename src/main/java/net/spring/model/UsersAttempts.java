package net.spring.model;

import java.util.Date;

/**
 * Created by sidorii on 15.03.17.
 */
public class UsersAttempts {

    private int id;
    private String username;
    private int attempts;
    private Date lastModifiedl;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int getAttempts() {
        return attempts;
    }

    public void setAttempts(int attempts) {
        this.attempts = attempts;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getLastModifiedl() {
        return lastModifiedl;
    }

    public void setLastModifiedl(Date lastModifiedl) {
        this.lastModifiedl = lastModifiedl;
    }
}
