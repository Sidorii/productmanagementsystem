package net.spring.model;


import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class Product implements Comparable<Product>{


    private Integer id;
    @NotNull
    @Size(min = 2,max = 100)
    private String name;
    private String producer;
    private Integer price;
    private String description;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProducer() {
        return producer;
    }

    public void setProducer(String producer) {
        this.producer = producer;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public int compareTo(Product o) {
        return (this.id - o.getId());
    }

    @Override
    public String toString() {
        return "Product: id: " + id + ", name: " + name + ", producer: " + producer + ", price: " + price;
    }
}
