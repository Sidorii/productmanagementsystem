package net.spring.controllers;

import net.spring.model.Product;
import net.spring.service.bussines.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.util.Set;
import java.util.TreeSet;


@Controller
@RequestMapping(value = "/account/")
public class ProductsController {


    private ProductService service;

    @Autowired
    public void setService(ProductService service) {
        this.service = service;
    }


    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ModelAndView helloPage(@RequestParam(name = "create_status", required = false) String createStatus,
                                  @RequestParam(name = "login", required = false) String login,
                                  @RequestParam(name = "updated_id", required = false) String updated,
                                  @RequestParam(name = "deleted_id", required = false) String deleted,
                                  @RequestParam(name = "style", required = false) String style) {

        Authentication auth =
                SecurityContextHolder.getContext().getAuthentication();

        String viewName = auth.getAuthorities().contains(new SimpleGrantedAuthority("ROLE_ADMIN")) ?
                "account/mainPage" : "account/mainPageUser";

        ModelAndView model = new ModelAndView(viewName);

        if (createStatus != null) {
            model.addObject("create", (createStatus.equals("OK")) ?
                    "New Product successfully created!" : "Error creating new product. Please, try again.");
        }

        if (login != null) {
            model.addObject("login", "Congratulations! You are successfully authorised.");
        }

        if (updated != null) {
            model.addObject("updated_id", (!updated.equals("ERROR")) ?
                    "Product with id " + updated + " is successfully updated." :
                    "Error updating product. Please, try again.");
        }

        if (deleted != null) {
            model.addObject("deleted_id", (!deleted.equals("ERROR")) ?
                    "Produce with id " + deleted + " is deleted." :
                    "Error deleting product. Please, try again.");
        }

        if (style != null)
            model.addObject("style", style);

        return model;
    }


    //return create page with special form
    @RequestMapping(value = "/create", method = RequestMethod.GET)
    public ModelAndView createGET(ModelAndView model) {
        model.addObject("product", new Product());
        model.setViewName("/account/createPage");

        return model;
    }

    //calls from create page and handle request (add entry in DB)
    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public ModelAndView createPOST(@ModelAttribute @Valid Product product, BindingResult bindingResult) {

        ModelAndView modelAndView = new ModelAndView("redirect:/account/");
        String status;
        String style;


        if (!bindingResult.hasErrors()) {
            service.addNewProduct(product);
            status = "OK";
            style = "msg";
        } else {
            status = "ERROR";
            style = "error";
        }
        modelAndView.addObject("create_status", status);
        modelAndView.addObject("style", style);

        return modelAndView;
    }

    //return update page with special form for this action
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public ModelAndView updateGET(@RequestParam(name = "product") Integer productId) {
        ModelAndView modelAndView = new ModelAndView();

        if (productId != null) {
            modelAndView.setViewName("/account/updatePage");
            modelAndView.addObject("product", service.getProduct(productId));
            modelAndView.addObject("style", "msg");
        } else {
            modelAndView.setViewName("account/mainPage");
            modelAndView.addObject("updated_id", "ERROR");
            modelAndView.addObject("style", "error");
        }
        return modelAndView;
    }

    //handle request from update page (update entry in DB)
    @RequestMapping(value = "/update/new", method = RequestMethod.POST)
    public ModelAndView updatePOST(@ModelAttribute @Valid Product product, BindingResult bindingResult) {

        ModelAndView modelAndView = new ModelAndView("redirect:/account/");
        String status;
        String style;

        if (!bindingResult.hasErrors()) {
            service.updateIfExistProduct(product);
            status = product.getId().toString();
            style = "msg";
        } else {
            status = "ERROR";
            style = "error";
        }
        modelAndView.addObject("updated_id", status);
        modelAndView.addObject("style", style);

        return modelAndView;
    }

    //delete entry from DB
    @RequestMapping(value = "/delete", method = RequestMethod.POST)
    public ModelAndView deleteGET(@RequestParam(name = "product_id", required = false) Integer productId) {
        ModelAndView modelAndView = new ModelAndView("redirect:/account/");
        if (productId != null) {
            service.deleteIfExistProduct(productId);
            modelAndView.addObject("deleted_id", productId.toString());
            modelAndView.addObject("style", "msg");
        } else {
            modelAndView.addObject("deleted_id", "ERROR");
            modelAndView.addObject("style", "error");

        }

        return modelAndView;
    }

    //get list of all products in db
    @ModelAttribute("productsList")
    public Set<Product> productList() {
        return new TreeSet<>(service.getAllProducts());
    }
}
