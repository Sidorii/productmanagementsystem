package net.spring.controllers;

import org.springframework.security.authentication.LockedException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by sidorii on 15.03.17.
 */
@Controller
public class AuthenticationController {


    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String redirect() {
        return "redirect:/account/";
    }


    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String index(@RequestParam(name = "error", required = false) String error,
                        @RequestParam(name = "logout", required = false) String logout, Model model,
                        HttpServletRequest request) {

        if (error != null) {
            if (error.equals("401"))
                model.addAttribute("error", getErrorMessage(request, "SPRING_SECURITY_LAST_EXCEPTION"));

            if (error.equals("403"))
                model.addAttribute("error", "Access Denied (but you authorized)");
        }
        if (logout != null)
            model.addAttribute("logout", "You are successfully login out");


        return "index";
    }

    private String getErrorMessage(HttpServletRequest httpServletRequest, String key) {
        Exception exception = (Exception) httpServletRequest.getSession().getAttribute(key);

        if (exception instanceof LockedException) {
            return exception.getMessage();
        }

        return "Invalid username or password!";
    }
}
