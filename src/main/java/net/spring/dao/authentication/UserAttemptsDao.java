package net.spring.dao.authentication;

import net.spring.model.UsersAttempts;

/**
 * Created by sidorii on 15.03.17.
 */
public interface UserAttemptsDao {


    void updateFailAttempts(String username);

    void resetFailAttempts(String username);

    UsersAttempts getUserAttempts(String username);

    void lockUser(String username, boolean isNotLocked);
}
