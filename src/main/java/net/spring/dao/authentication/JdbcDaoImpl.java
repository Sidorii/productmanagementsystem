package net.spring.dao.authentication;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by sidorii on 15.03.17.
 */
public class JdbcDaoImpl extends org.springframework.security.core.userdetails.jdbc.JdbcDaoImpl
        implements UserDetailsService {


    private static final String QUERY =
            "SELECT username,password,enabled,user_attempts.is_user_not_lock AS not_lock " +
            "FROM users " +
            "INNER JOIN user_attempts " +
            "ON users.id = user_attempts.user_id " +
            "WHERE username = ?";


    @Override
    @Value("SELECT users.username AS username, name AS my_role FROM roles " +
            "INNER JOIN users_roles " +
            " ON roles.id = users_roles.role_id " +
            "INNER JOIN users " +
            "ON users.id = users_roles.user_id " +
            "WHERE username = ?")
    public void setAuthoritiesByUsernameQuery(String queryString) {
        super.setAuthoritiesByUsernameQuery(queryString);
    }




    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        return getJdbcTemplate().queryForObject(QUERY,
                new Object[]{username},

                (RowMapper<UserDetails>) (rs, rowNum) -> {

                    String userName = rs.getString("username");
                    String password = rs.getString("password");
                    boolean enabled = rs.getBoolean("enabled");
                    boolean isUserNotLocked = rs.getBoolean("not_lock");

                    return new User(userName, password, enabled, true, true,
                            isUserNotLocked, loadGroupAuthorities(username));
                });
    }


    @Override
    protected List<GrantedAuthority> loadGroupAuthorities(String username) {

        SqlRowSet rs = getJdbcTemplate().queryForRowSet(getAuthoritiesByUsernameQuery(), username);
        List<GrantedAuthority> result = new ArrayList<>();

        while (rs.next())
            result.add(new SimpleGrantedAuthority(rs.getString("my_role")));

        return result;
    }
}
