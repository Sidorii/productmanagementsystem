package net.spring.dao.authentication;

import net.spring.model.UsersAttempts;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.security.core.userdetails.jdbc.JdbcDaoImpl;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;
import java.util.Date;


/**
 * Created by sidorii on 15.03.17.
 */
public class UserAttemptsDaoImpl extends JdbcDaoImpl implements UserAttemptsDao {

    private static final String UPDATE_ATTEMPTS_QUERY =
            "UPDATE user_attempts SET count = count + 1, last_modify = ? " +
                    "WHERE user_id = (SELECT id FROM users WHERE username = ?)";

    private static final String RESET_ATTEMPTS_QUERY =
            "UPDATE user_attempts SET count = 0 " +
                    "WHERE user_id = (SELECT id FROM users WHERE username = ?)";

    private static final String USER_ATTEMPTS_QUERY =
            "SELECT users.id AS id ,users.username AS username, user_attempts.count AS count," +
                    " user_attempts.last_modify AS last_modify " +
                    "FROM user_attempts " +
                    "INNER JOIN users ON user_attempts.user_id = users.id " +
                    "WHERE username = ?";


    @Autowired
    private DataSource dataSource;


    @PostConstruct
    private void initialize() {
        setDataSource(dataSource);
    }

    @Override
    public void updateFailAttempts(String username) {
        getJdbcTemplate().update(UPDATE_ATTEMPTS_QUERY,
                new Date(), username);
    }

    @Override
    public void resetFailAttempts(String username) {
        getJdbcTemplate().update(RESET_ATTEMPTS_QUERY, username);
    }

    @Override
    public UsersAttempts getUserAttempts(String username) {

        SqlRowSet rs = getJdbcTemplate().queryForRowSet(USER_ATTEMPTS_QUERY, username);

        if (rs.next()) {
            UsersAttempts user = new UsersAttempts();

            user.setId(rs.getInt("id"));
            user.setAttempts(rs.getInt("count"));
            user.setLastModifiedl(rs.getDate("last_modify"));
            user.setUsername(rs.getString("username"));
            return user;
        }

        return null;
    }

    @Override
    public void lockUser(String username, boolean isNotLocked) {
        getJdbcTemplate().update("UPDATE user_attempts SET is_user_not_lock = ? WHERE user_id = " +
                "(SELECT id FROM users WHERE username = ?);", isNotLocked, username);
    }
}
