package net.spring.dao.bussines;

import net.spring.model.Product;

import java.util.List;

/**
 * Created by sidorii on 17.03.17.
 */
public interface ProductDao {


    void addProduct(Product product);

    Product getProduct(int id);

    void updateProduct(Product product);

    void deleteProduct(int id);

    List<Product> getAllProducts();

}
