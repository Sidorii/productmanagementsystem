package net.spring.dao.bussines;


public enum ProductQueries {

    ADD(
            "INSERT INTO products(name, producer, price, description) " +
            "VALUES (?,?,?,?)"
    ),
    GET(
            "SELECT * FROM products WHERE id=?"
    ),
    UPDATE(
            "UPDATE products SET name=?,producer=?,price=?,description=? " +
                    "WHERE id=?"
    ),
    DELETE(
            "DELETE FROM products WHERE id=?"
    ),
    GET_ALL(
            "SELECT * FROM products"
    );


    private String query;

    ProductQueries(String query){
        this.query = query;
    }

    @Override
    public String toString() {
        return query;
    }
}
