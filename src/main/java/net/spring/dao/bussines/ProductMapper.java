package net.spring.dao.bussines;

import net.spring.model.Product;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by sidorii on 17.03.17.
 */
public class ProductMapper implements RowMapper<Product> {

    @Override
    public Product mapRow(ResultSet rs, int rowNum) throws SQLException {
        Product product = new Product();
        product.setId(rs.getInt("id"));
        product.setName(rs.getString("name"));
        product.setProducer(rs.getString("producer"));
        product.setPrice(rs.getInt("price"));
        product.setDescription(rs.getString("description"));

        return product;
    }

}
