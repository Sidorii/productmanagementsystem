package net.spring.dao.bussines;

import net.spring.model.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;


public class ProductDaoImpl implements ProductDao {


    private JdbcTemplate jdbcTemplate;


    @Autowired
    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public void addProduct(Product product) {

        Object[] parameters = new Object[]{
                product.getName(), product.getProducer(),
                product.getPrice(), product.getDescription()
        };

        jdbcTemplate.update(ProductQueries.ADD.toString(), parameters);
    }

    @Override
    public Product getProduct(int id) {
        return jdbcTemplate.queryForObject(ProductQueries.GET.toString(), new Object[]{id}, new ProductMapper());
    }

    @Override
    public void updateProduct(Product product) {
        Object[] parameters = new Object[]{
                product.getName(), product.getProducer(),
                product.getPrice(), product.getDescription(), product.getId()
        };

        jdbcTemplate.update(ProductQueries.UPDATE.toString(), parameters);
    }

    @Override
    public void deleteProduct(int id) {
        jdbcTemplate.update(ProductQueries.DELETE.toString(), id);
    }

    @Override
    public List<Product> getAllProducts() {
        return jdbcTemplate.query(ProductQueries.GET_ALL.toString(), new ProductMapper());
    }
}
